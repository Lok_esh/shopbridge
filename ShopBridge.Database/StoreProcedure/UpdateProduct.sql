﻿CREATE DEFINER=`root`@`localhost` PROCEDURE `UpdateProduct`(
	IN `id` int,
	IN `productName` varchar(50),
	IN `brand` varchar(50),
	IN `weight` int,
	IN `price` int,
	IN `MfgDate` varchar(50),
	IN `description` varchar(50) 


)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
UPDATE product SET ProductName=productName,Brand=brand,Weight=weight,Price=price,MfgDate=MfgDate,Description=description WHERE product.Id = id