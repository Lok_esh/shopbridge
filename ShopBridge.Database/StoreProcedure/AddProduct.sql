﻿CREATE DEFINER=`root`@`localhost` PROCEDURE `AddProduct`(
	IN `productName` varchar(50),
	IN `brand` varchar(50),
	IN `weight` int,
	IN `price` int,
	IN `MfgDate` varchar(50),
	IN `description` varchar(50) 


)
LANGUAGE SQL
NOT DETERMINISTIC
CONTAINS SQL
SQL SECURITY DEFINER
COMMENT ''
insert into product (ProductName,Brand,Weight,Price,MfgDate,Description) 
 values (productName ,brand,weight,price,MfgDate,description)