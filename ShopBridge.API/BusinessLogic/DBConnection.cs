﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace ShopBridge.API.BusinessLogic
{
    public sealed class DBConnection
    {
        private static volatile MySqlConnection instance;
        private static readonly string connectionString = ConfigurationManager.ConnectionStrings["Mysqlconnectionstring"].ConnectionString;

        private DBConnection() { }

        public static MySqlConnection connection
        {
            get
            {
                try
                {
                    if (instance == null)
                    {
                        instance = new MySqlConnection(connectionString);
                        instance.Open();
                    }
                    else
                    {
                        instance.ConnectionString = connectionString;
                        instance.Open();
                    }
                    return instance;
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }
    }
}