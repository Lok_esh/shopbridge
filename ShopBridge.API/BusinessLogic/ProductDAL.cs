﻿using MySql.Data.MySqlClient;
using ShopBridge.API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ShopBridge.API.BusinessLogic
{
    public class ProductDAL
    {
        public List<Product> getProduct()
        {
            List<Product> lstForProduct = new List<Product>();
            try
            {
                using (MySqlConnection conn = DBConnection.connection)
                {
                    using (MySqlCommand cmd = new MySqlCommand("GetProduct",conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        using(MySqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                lstForProduct.Add(new Product
                                {
                                    id = Convert.ToInt32(reader[0]),
                                    productName = Convert.ToString(reader[1]),
                                    brand = Convert.ToString(reader[2]),
                                    weight = Convert.ToInt32(reader[3]),
                                    price = Convert.ToInt32(reader[4]),
                                    mfgDate = Convert.ToString(reader[5]),
                                    description = Convert.ToString(reader[6])
                                });
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return lstForProduct;
        }

        public bool AddProduct(Product prod)
        {
            bool flag = false;
            try
            {
                using (MySqlConnection conn = DBConnection.connection)
                {
                    using (MySqlCommand cmd = new MySqlCommand("Addproduct", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@productName",prod.productName);
                        cmd.Parameters.AddWithValue("@brand",prod.brand);
                        cmd.Parameters.AddWithValue("@weight",prod.weight);
                        cmd.Parameters.AddWithValue("@price",prod.price);
                        cmd.Parameters.AddWithValue("@MfgDate",prod.mfgDate);
                        cmd.Parameters.AddWithValue("@description",prod.description);
                        cmd.ExecuteNonQuery();
                        flag = true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return flag;
        }

        public bool UpdateProduct(Product prod)
        {
            bool flag = false;
            try
            {
                using (MySqlConnection conn = DBConnection.connection)
                {
                    using (MySqlCommand cmd = new MySqlCommand("UpdateProduct", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@id", prod.id);
                        cmd.Parameters.AddWithValue("@productName", prod.productName);
                        cmd.Parameters.AddWithValue("@brand", prod.brand);
                        cmd.Parameters.AddWithValue("@weight", prod.weight);
                        cmd.Parameters.AddWithValue("@price", prod.price);
                        cmd.Parameters.AddWithValue("@MfgDate", prod.mfgDate);
                        cmd.Parameters.AddWithValue("@description", prod.description);
                        cmd.ExecuteNonQuery();
                        flag = true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return flag;
        }

        public bool DeleteProduct(int id)
        {
            bool flag = false;
            try
            {
                using (MySqlConnection conn = DBConnection.connection)
                {
                    using (MySqlCommand cmd = new MySqlCommand("DeleteProduct", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@id",id);
                        cmd.ExecuteNonQuery();
                        flag = true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return flag;
        }
    }
}