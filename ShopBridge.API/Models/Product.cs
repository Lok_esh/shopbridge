﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShopBridge.API.Models
{
    public class Product
    {
        public int id { get; set; }

        public string productName { get; set; }

        public string brand { get; set; }

        public string description { get; set; }

        public int price { get; set; }

        public int weight { get; set; }

        public string mfgDate { get; set; }

    }
}