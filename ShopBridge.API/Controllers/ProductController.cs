﻿using ShopBridge.API.BusinessLogic;
using ShopBridge.API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using ShopBridge.API.Logs;

namespace ShopBridge.API.Controllers
{
    public class ProductController : Controller
    {
        ProductDAL productObj;
        public ProductController()
        {
            productObj = new ProductDAL();
        }
        // GET: Product
        public async Task<ActionResult> Index()
        {

            Logs.Logs.InfoLogs("Inside ProductController : Index Action : Start the process", DateTime.Now.ToString());

            List<Product> lstProduct = new List<Product>();
            try
            {
                Logs.Logs.InfoLogs("Inside ProductController : Index Action : Execute the getProduct() method", DateTime.Now.ToString());
                lstProduct = productObj.getProduct();
            }
            catch (Exception ex)
            {
                Logs.Logs.ErrorLogs(ex.Message.ToString(), DateTime.Now.ToString());
            }
            Logs.Logs.InfoLogs("Inside ProductController : Index Action : End the process", DateTime.Now.ToString());
            return Json(lstProduct, JsonRequestBehavior.AllowGet);  
                
        }


        [System.Web.Http.HttpPost]
        public async Task<ActionResult> AddProduct([FromBody] Product prod)
        {
            Logs.Logs.InfoLogs("Inside ProductController : AddProduct Action : Start the process", DateTime.Now.ToString());
            bool result=false;
            try
            {
                Logs.Logs.InfoLogs("Inside ProductController : AddProduct Action : Execute the AddProduct() method", DateTime.Now.ToString());          
                result = productObj.AddProduct(prod);
            }
            catch (Exception ex)
            {
                Logs.Logs.ErrorLogs(ex.Message.ToString(),DateTime.Now.ToString());
            }
            Logs.Logs.InfoLogs("Inside ProductController : AddProduct Action : End the process", DateTime.Now.ToString());
            return Json(result);

        }

        [System.Web.Http.HttpPost]
        public async Task<ActionResult> UpdateProduct([FromBody] Product prod)
        {
            Logs.Logs.InfoLogs("Inside ProductController : UpdateProduct Action : Start the process", DateTime.Now.ToString());
            List<Product> lstProduct = new List<Product>();

            bool result = false;
            try
            {
                Logs.Logs.InfoLogs("Inside ProductController : UpdateProduct Action : Execute getProduct() method", DateTime.Now.ToString());
                lstProduct = productObj.getProduct();
                dynamic lst = lstProduct.Where(t => t.id == prod.id).ToList();
                if (string.IsNullOrEmpty(prod.productName))
                {
                    prod.productName = lst[0].productName;
                }
                if (string.IsNullOrEmpty(prod.brand))
                {
                    prod.brand = lst[0].brand;
                }
                if (prod.weight==0)
                {
                    prod.weight = lst[0].weight;
                }
                if (prod.price==0)
                {
                    prod.price = lst[0].price;
                }
                if (string.IsNullOrEmpty(prod.mfgDate))
                {
                    prod.mfgDate = lst[0].mfgDate;
                }
                if (string.IsNullOrEmpty(prod.description))
                {
                    prod.description = lst[0].description;
                }
                Logs.Logs.InfoLogs("Inside ProductController : UpdateProduct Action : Execute UpdateProduct() method", DateTime.Now.ToString());
                result = productObj.UpdateProduct(prod);

            }
            catch (Exception ex)
            {
                Logs.Logs.ErrorLogs(ex.Message.ToString(), DateTime.Now.ToString());
            }
            Logs.Logs.InfoLogs("Inside ProductController : UpdateProduct Action : End the process", DateTime.Now.ToString());
            return Json(result);

        }

        [System.Web.Http.HttpPost]
        public async Task<ActionResult> DeleteProduct(int id)
        {
            Logs.Logs.InfoLogs("Inside ProductController : DeleteProduct Action : Start the process", DateTime.Now.ToString());
            List<Product> lstProduct = new List<Product>();

            bool result = false;
            try
            {
                Logs.Logs.InfoLogs("Inside ProductController : DeleteProduct Action :Execute the DeleteProduct() method", DateTime.Now.ToString());

                result = productObj.DeleteProduct(id);

            }
            catch (Exception ex)
            {
                Logs.Logs.ErrorLogs(ex.Message.ToString(), DateTime.Now.ToString());
            }
            Logs.Logs.InfoLogs("Inside ProductController : DeleteProduct Action :End the Processs", DateTime.Now.ToString());
            return Json(result);

        }
    }
}