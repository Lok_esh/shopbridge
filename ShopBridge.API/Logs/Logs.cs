﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace ShopBridge.API.Logs
{
    public static class Logs
    {
        public static readonly string docPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
        public static readonly string info = docPath + "\\Shopbridge\\info.txt";
        public static readonly string error = docPath + "\\Shopbridge\\error.txt";
        static Logs()
        {
            if (!Directory.Exists(docPath + "\\Shopbridge"))
            {
                Directory.CreateDirectory(docPath + "\\Shopbridge");
            }

            //if(!File.Exists(info))
            //{
            //    File.Create(info);
            //}
            //if (!File.Exists(error))
            //{
            //    File.Create(error);
            //}
        }

        public static void InfoLogs(string infoData,string date)
        {
            File.AppendAllText(info,"\nDateTime: "+ date + "Content : " +infoData);
        }

        public static void ErrorLogs(string erroData, string date)
        {
            File.AppendAllText(info, "\nDateTime: " + date + "Content : " + erroData);
        }
    }
}